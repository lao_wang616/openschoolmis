define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'school/grade/index' + location.search,
                    add_url: 'school/grade/add',
                    edit_url: 'school/grade/edit',
                    del_url: 'school/grade/del',
                    multi_url: 'school/grade/multi',
                    addBk_url: 'school/grade/addBk',
                    import_url: 'school/grade/import',
                    table: 'grade',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                queryParams : function (params) {
                    var filter = JSON.parse(params.filter);
                    var op = JSON.parse(params.op);
                    filter['s.student_number'] = filter.student_id_text;
                    delete filter.student_id_text;
                    op['s.student_number'] = op.student_id_text;
                    params.filter = JSON.stringify(filter);
                    params.op = JSON.stringify(op);
                    return params;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), visible:false, operate: false},
                        {field: 'student_id_text', title: __('Student_id'), operate:"LIKE"},
                        {field: 'student_name', title: __('Student_name'), operate:"LIKE"},
                        {field: 'major_id', title: __('Major_id'),searchList: $.getJSON("school/major/searchlist"),visible:false},//只用于查询（不显示列表），查询是where必须是字段名.admin\controller\school
                        {field: 'major_id_text', title: __('Major_id'),operate:false},//只用于展示，不显示在条件查询中
                        {field: 'lesson_id', title: __('Lesson_id'),searchList: $.getJSON("school/lesson/searchlist"),visible:false},//只用于查询（不显示列表），查询是where必须是字段名.admin\controller\school
                        {field: 'lesson_id_text', title: __('Lesson_id'),operate:false},//只用于展示，不显示在条件查询中
                        {field: 'major_level_dict', title: __('Major_level_dict'), searchList: Config.majorLevelDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        {field: 'level', title: __('Level')},
                        {field: 'term_dict', title: __('Term_dict'), searchList: Config.termDictList, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        {field: 'sex', title: __('Sex'), searchList: {"0":__('Sex 0'),"1":__('Sex 1')}, formatter: Table.api.formatter.normal},
                        {field: 'jm_score', title: __('Jm_score'), operate:'BETWEEN',visible:false, operate: false},
                        {field: 'pd_score', title: __('Pd_score'), operate:'BETWEEN',visible:false, operate: false},
                        {field: 'cs_score', title: __('Cs_score'), operate:'BETWEEN',visible:false, operate: false},
                        {field: 'sy_score', title: __('Sy_score'), operate:'BETWEEN',visible:false, operate: false},
                        {field: 'ps_score', title: __('Ps_score'), operate:'BETWEEN',visible:false, operate: false},
                        {field: 'cq_score', title: __('Cq_score'), operate:'BETWEEN',visible:false, operate: false},
                        {field: 'sx_score', title: __('Sx_score'), operate:'BETWEEN',visible:false, operate: false},
                        {field: 'score', title: __('Score'), operate:'BETWEEN'},
                        {field: 'is_bukao', title: __('Is_bukao'), searchList: {"0":__('Is_bukao 0'),"1":__('Is_bukao 1')}, formatter: Table.api.formatter.normal},
                        {field: 'bk_score', title: __('Bk_score'), operate: false},
                        {field: 'cx_score', title: __('Cx_score'), operate: false},
                        {field: 'create_by', title: __('Create_by'),visible:false, operate: false},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime,visible:false},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime,visible:false, operate: false},
                        {field: 'remark', title: __('Remark'),visible:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(".btn-addBk").on("click",function(){
                Backend.api.open('school/grade/addBk', '添加补考成绩', '');
            });

            $(".btn-download").on("click",function(){
                window.location.href = "/excel_template/学生成绩导入模板.xlsx";
            });

            
            $(".btn-download-model").on("click",function(){
                window.location.href = "grade/exportModel";
            });
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'school/grade/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'school/grade/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'school/grade/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            require(['selectpage'], function () {
                $('#c-student_id').selectPage({
                    eAjaxSuccess: function (data) {

                        data.list = typeof data.rows !== 'undefined' ? data.rows : (typeof data.list !== 'undefined' ? data.list : []);
                        data.totalRow = typeof data.total !== 'undefined' ? data.total : (typeof data.totalRow !== 'undefined' ? data.totalRow : data.list.length);
                        return data;

                    },
                    eSelect: function (data) {
                        $("#c-student_name").val(data.name);
                        $("#c-major_id").val(data.major_id).selectPageRefresh();
                        $("#c-level").val(data.level); 
                        $("#c-major_level_dict").val(data.major_level_dict).selectpicker('render');
                        $("#c-sex").val(data.student_sex).selectpicker('render');
                        $("#lesson_ids").val(data.lesson_ids);
                        // $("#c-lesson_id").data("params",'{"custom[id]":["in","'+data.lesson_ids+'"]}');

                    }
                });
            });
            $("#c-is_bukao").on("change",function(){
                if($("#c-is_bukao").val() == "1"){
                    $(".isbukao").show();
                }else{
                    $(".isbukao").hide();
                }
            });
            $("#c-lesson_id").data("params", function (obj) {
                return {custom: {id: ["in",$("#lesson_ids").val()]}};
            });

            Controller.api.bindevent();

            
        },
        addbk: function () {
            require(['selectpage'], function () {
                $('#c-student_id').selectPage({
                    eAjaxSuccess: function (data) {

                        data.list = typeof data.rows !== 'undefined' ? data.rows : (typeof data.list !== 'undefined' ? data.list : []);
                        data.totalRow = typeof data.total !== 'undefined' ? data.total : (typeof data.totalRow !== 'undefined' ? data.totalRow : data.list.length);
                        return data;

                    },
                    eSelect: function (data) {
                        $("#c-student_name").val(data.name);
                        $("#c-major_id").val(data.major_id).selectPageRefresh();
                        $("#c-level").val(data.level); 
                        $("#c-major_level_dict").val(data.major_level_dict).selectpicker('render');
                        $("#c-sex").val(data.student_sex).selectpicker('render');
                    }
                });
            });
            Controller.api.bindevent();
        },
        edit: function () {
            if($("#c-is_bukao").val() == "1"){
                $(".isbukao").show();
            }else{
                $(".isbukao").hide();
            }
            $("#c-is_bukao").on("change",function(){
                if($("#c-is_bukao").val() == "1"){
                    $(".isbukao").show();
                }else{
                    $(".isbukao").hide();
                }
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});

function eSelect(data) {
    console.log(1111)
    debugger

}