<?php

return array (
  'name' => '网非教学管理平台',
  'beian' => '',
  'cdnurl' => '',
  'version' => '1.0.1',
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' => 
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' => 
  array (
    'default' => 'Default',
    'page' => 'Page',
    'article' => 'Article',
    'test' => 'Test',
  ),
  'configgroup' => 
  array (
    'basic' => 'Basic',
    'email' => 'Email',
    'dictionary' => 'Dictionary',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => 'smtp.163.com',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => 'chaojitest1@163.com',
  'mail_smtp_pass' => 'spring122333',
  'mail_verify_type' => '2',
  'mail_from' => 'chaojitest1@163.com',
  'work_station' => 
  array (
    1 => '北京站',
    2 => '青岛站',
  ),
  'major_level' => 
  array (
    1 => '高升本',
    2 => '专升本',
    3 => '成人高考',
  ),
  'term' => 
  array (
    1 => '第一学期',
    2 => '第二学期',
    3 => '第三学期',
    4 => '第四学期',
  ),
  'student_last_level' => 
  array (
    1 => '小学',
    2 => '初中',
    3 => '高中',
    4 => '专科',
    5 => '本科',
    6 => '硕士研究生',
  ),
  'student_political_status' => 
  array (
    1 => '群众',
    2 => '中共党员',
    3 => '中共预备党员',
    4 => '共青团员',
    5 => '其他',
  ),
  'student_last_type' => 
  array (
    1 => '全日制',
    2 => '非全日制',
  ),
  'student_type' => 
  array (
    1 => '全日制',
    2 => '非全日制',
  ),
  'class_time' => 
  array (
    '上午' => '9:00~12：00',
    '下午' => '13:00~16:00',
  ),
  'room' => 
  array (
    1 => '101',
    2 => '102',
    3 => '103',
  ),
);