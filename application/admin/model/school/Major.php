<?php

namespace app\admin\model\school;

use think\Model;
use think\Config;
use traits\model\SoftDelete;

class Major extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'major';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'major_level_dict_text',
        'work_station_dict_text',
        'lesson_ids_text',
        'status_text'
    ];
    

    
    public function getMajorLevelDictList()
    {
        return Config::get("site.major_level");
    }

    public function getWorkStationDictList()
    {
        return Config::get("site.work_station");
    }

    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1')];
    }

    public function getLessonIdsTextAttr($value, $data)
    {
        $lessons = (new Lesson())->field("name")->where("id", "in", $data['lesson_ids'])->select();

        return implode(",", array_column($lessons, 'name'));
    }


    public function getMajorLevelDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['major_level_dict']) ? $data['major_level_dict'] : '');
        $list = $this->getMajorLevelDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getWorkStationDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['work_station_dict']) ? $data['work_station_dict'] : '');
        $list = $this->getWorkStationDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIdByName($name)
    {
        $r = $this->field("id")->where("name", $name)->find();
        return $r['id'];
    }
    //根据专业id获取到专业详情
    public function getMajor($data)
    {
        $major =$this->where("id", $data)->find();
        return $major;
    }

    public function getLessonList($value, $data)
    {
        $lessons = (new Lesson())->where("id", "in", $data['lesson_ids'])->select();

        return $lessons;
    }

    public function getTotalNum()
    {
        $num = $this->count();
        return $num;
    }
}
