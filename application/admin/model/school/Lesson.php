<?php

namespace app\admin\model\school;

use think\Model;
use think\Config;
use traits\model\SoftDelete;

class Lesson extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'lesson';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'lesson_score_type_text',
        'status_text'
    ];
    

    
    public function getLessonScoreTypeList()
    {
        return ['0' => __('Lesson_score_type 0'), '1' => __('Lesson_score_type 1')];
    }

    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1')];
    }


    public function getLessonScoreTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['lesson_score_type']) ? $data['lesson_score_type'] : '');
        $list = $this->getLessonScoreTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getTotalNum()
    {
        $num = $this->count();
        return $num;
    }


}
