<?php

namespace app\admin\model\school;

use think\Model;
use think\Config;
use traits\model\SoftDelete;

class Grade extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'grade';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'major_level_dict_text',
        'term_dict_text',
        'sex_text',
        'lesson_id_text',
        'major_id_text',
        'student_id_text',
        'is_bukao_text'
    ];
    

    
    public function getMajorLevelDictList()
    {
        return Config::get("site.major_level");
    }

    public function getTermDictList()
    {
        return Config::get("site.term");
    }

    public function getSexList()
    {
        return ['0' => __('Sex 0'), '1' => __('Sex 1')];
    }

    public function getIsBukaoList()
    {
        return ['0' => __('Is_bukao 0'), '1' => __('Is_bukao 1')];
    }


    public function getMajorLevelDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['major_level_dict']) ? $data['major_level_dict'] : '');
        $list = $this->getMajorLevelDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getTermDictTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['term_dict']) ? $data['term_dict'] : '');
        $list = $this->getTermDictList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getSexTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sex']) ? $data['sex'] : '');
        $list = $this->getSexList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsBukaoTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_bukao']) ? $data['is_bukao'] : '');
        $list = $this->getIsBukaoList();
        return isset($list[$value]) ? $list[$value] : '';
    }
    
    public function getLessonIdTextAttr($value, $data)
    { 
        $lessons = (new Lesson())->field("name")->where("id", $data['lesson_id'])->find();

        return  $lessons['name'];
    }

    public function getMajorIdTextAttr($value, $data)
    { 
        $major = (new Major())->field("name")->where("id", $data['major_id'])->find();

        return  $major['name'];
    }

    public function getStudentIdTextAttr($value, $data)
    { 
        $student = (new Student())->field("student_number")->where("id", $data['student_id'])->find();

        return  $student['student_number'];
    }

    

}
