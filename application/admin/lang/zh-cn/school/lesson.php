<?php

return [
    'Id'                  => '课程id',
    'Name'                => '课程名称',
    'Lesson_score'        => '学分',
    'Lesson_hour'         => '课时',
    'Lesson_score_type'   => '计分模式(单选)',
    'Lesson_score_type 0' => '等级式',
    'Lesson_score_type 1' => '数值式',
    'Status'              => '状态',
    'Status 0'            => '停课',
    'Status 1'            => '正常',
    'Create_by'           => '创建者',
    'Createtime'          => '创建时间',
    'Updatetime'          => '更新时间',
    'Deletetime'          => '删除时间',
    'Remark'              => '备注信息'
];
